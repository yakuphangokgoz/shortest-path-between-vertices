﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.UI;


/**
 * "Logic" script implements the required logic to solve the "Shortest Path Between Vertices" problem.
 * On solving the problem, graph theory and Dijkstra's shortest path algorithm was used.
 * 
 * Dividing the problem into pieces, how the resources were referred explained below:
 * 
 *     => Mesh was referred as Graph
 *     => Vertex was referred as Node
 *     => Edge was referred as Edge
 * 
 * Author: Yakuphan Gökgöz
 * 
 */




/**
 * <summary>
 * "Node" class represents the nodes in graph which are the vertices in mesh.
 * </summary>
 */
public class Node
{
    public Vector3 position; //Position of vertex
    public Dictionary<Node, float> neighbors; //Neigbors of the vertex and their distances

    public Node(Vector3 position)
    {
        this.position = position;
        neighbors = new Dictionary<Node, float>();
    }

    public Node(Vector3 position, Dictionary<Node, float> neighbors)
    {
        this.position = position;
        this.neighbors = neighbors;
    }
}

/**
 * <summary>
 * "Edge" class represents the edges in graph and in mesh.
 * </summary>
 */
public class Edge
{
    public Node vertex1; //One end of the edge
    public Node vertex2; //Other end of the edge
    public float length; //Length of the edge
    
    public Edge(Node vertex1, Node vertex2)
    {
        this.vertex1 = vertex1;
        this.vertex2 = vertex2;
        this.length = Vector3.Distance(this.vertex1.position, this.vertex2.position);
    }

}

/**
 * <summary>
 * "MeshGraph" class represents the graph which is constructed on the mesh data.
 * </summary>
 */
public class MeshGraph
{
    public List<Node> nodes; //Node list of graph (vertice of mesh)
    public List<Edge> edges; //Edge list of graph (edges of mesh)

    public MeshGraph(List<Node> nodes, List<Edge> edges)
    {
        this.nodes = nodes;
        this.edges = edges;
    }
}



/**
 * <summary>
 * "Dijkstra" class represents the Dikstra algorithm on shortest path problem.
 * It implements the required collections and functions which construct the algorithm.
 * </summary>
 */
public class Dijkstra
{
    public Dictionary<Node, float> distances; //Distance map of each node
    public Dictionary<Node, Node> routes; //Node pairs (nodes pointing each other) which represent the route between source and destination node
    MeshGraph graph; //Graph that consists of mesh data (vertices and edges)
    List<Node> nodePool; //Node queue that will be iterated through during algorithm

    public Dijkstra(MeshGraph graph)
    {
        this.graph = graph;
        this.nodePool = new List<Node>(graph.nodes);
        distances = InitializeDistances();
        routes = InitializeRoutes();
    }

    /**
     * <summary>
     * "InitializeDistances" function sets all node distances to infinity.
     * </summary>
     * 
     * <returns>
     * 
     * Type: Dictionary<Node, float>
     * Distance dictionary
     * 
     * </returns>
     */
    Dictionary<Node, float> InitializeDistances()
    {
        Dictionary<Node, float> distances = new Dictionary<Node, float>();

        foreach (Node n in graph.nodes)
        {
            distances.Add(n, float.MaxValue);
        }
        return distances;
    }

    /**
     * <summary>
     * "InitializeRoutes" function sets all node routes to null.
     * </summary>
     * 
     * <returns>
     * 
     * Type: Dictionary<Node, Node>
     * Route dictionary
     * 
     * </returns>
     * 
     */
    Dictionary<Node, Node> InitializeRoutes()
    {
        Dictionary<Node, Node> routes = new Dictionary<Node, Node>();

        foreach (Node n in graph.nodes)
        {
            routes.Add(n, null);
        }
        return routes;
    }

    /**
     * <summary>
     * "GetClosestNode" function finds the node with closest distance in node pool.
     * </summary>
     * 
     * <returns>
     * 
     * Type: Node
     * The closest node in node pool
     * 
     * </returns>
     */
    Node GetClosestNode()
    {
        Node closest = nodePool.FirstOrDefault();

        foreach (Node node in nodePool)
        {
            if (distances[node] < distances[closest])
                closest = node;
        }

        return closest;
    }


    /**
     * <summary>
     * "CheckNeighbors" function checks the parameter node's neighbor distances and
     * if it finds a better route, it updates the distance and the route dictionaries.
     * </summary>
     * 
     * <param name="node">
     * The node whose neighbors will be checked.
     * </param>
     * 
     */
    void CheckNeighbors(Node node)
    {
        foreach (KeyValuePair<Node,float> neighbor in node.neighbors)
        {
            if (distances[node] + neighbor.Value < distances[neighbor.Key])
            {
                distances[neighbor.Key] = neighbor.Value + distances[node];
                routes[neighbor.Key] = node;
            }
        }
    }


    /**
     *<summary>
     * "Apply" function is the main function that runs the simulation of the algorithm 
     * </summary> 
     * 
     * <param name="source">
     * Source node.
     * </param>
     * 
     * <param name="destination">
     * Destination node.
     * </param>
     */
    public void Apply(Node source, Node destination)
    {
        distances[source] = 0;

        while (nodePool.Count > 0)
        {
            Node closestNode = GetClosestNode();

            //This flow control block terminates the algorithm simulation when it reaches
            //the destination node for optimization purposes.
            //For a complete calculation, it can be commented out.
            if (closestNode.Equals(destination))
            {
                break;
            }
            CheckNeighbors(closestNode);
            nodePool.Remove(closestNode);
        }

    }

    /**
     * <summary>
     * "SetRoutePositions" function adds node positions on the route to "positions" list parameter recursively,
     * starting from "node" parameter.
     * </summary>
     * 
     * <param name="node">
     * Node that the recursion will start from.
     * </param>
     * 
     * <param name="positions">
     * List that node positions will be added.
     * </param>
     */
    public void SetRoutePositions(Node node, List<Vector3> positions)
    {
        if (routes[node] == null)
        {
            positions.Add(node.position);
            return;
        }
        positions.Add(node.position);
        SetRoutePositions(routes[node], positions);
        
    }

}


public class Logic : MonoBehaviour
{
    /**
     * Base game object that will be spawned on vertices.
     */
    public GameObject sphere;

    /**
     * Base game object that will be spawned on route edges.
     */
    public GameObject line;

    /**
     * Mesh data.
     */
    MeshFilter characterMeshFilter;
    Mesh characterMesh;
    Vector3[] characterVertices;
    int[] characterMeshTriangles;

    /**
     * Graph data which will be constructed from mesh data.
     */
    List<Node> graphNodes;
    List<Edge> graphEdges;
    MeshGraph meshGraph;

    /**
     * Algorithm object.
     */
    Dijkstra dijkstra;

    /**
     * List that will hold the vertex positions on the route from source node to destination node.
     */
    List<Vector3> routePositions;

    /**
     * Game object reference that will be spawned on source vertex.
     */
    GameObject startSphere;

    /**
     * Game object reference that will be spawned on destination vertex.
     */
    GameObject endSphere;

    /**
     * Game object reference that will be spawned on route edges.
     */
    GameObject lineObject;

    /**
    * Stat interface.
    */
    public Text nodeCountText;
    public Text calcDurationText;

    /**
     * Ray and raycast hit variables which are used for selecting the source and destination vertices.
     */
    Ray ray;
    RaycastHit hit;

    /**
     * <summary>
     * "AnalyzeMeshData" function collects required mesh data to construct the graph.
     * </summary> 
     */
    void AnalyzeMeshData()
    {
        characterMeshFilter = GetComponent<MeshFilter>();
        characterMesh = characterMeshFilter.mesh;
        characterVertices = characterMesh.vertices;
        characterMeshTriangles = characterMesh.triangles;
    }

    /**
     * <summary>
     * "CreateGraphNodes" function creates graph nodes from mesh vertices.
     * </summary> 
     */
    void CreateGraphNodes()
    {
        graphNodes = new List<Node>();
        for (int i = 0; i < characterVertices.Length; i++)
        {
            graphNodes.Add(new Node(characterVertices[i]));
        }
    }

    /**
     * <summary>
     * "CreateGraphEdges" function creates graph edges by using mesh triangle information.
     * </summary> 
     */
    void CreateGraphEdges()
    {
        graphEdges = new List<Edge>();
        for (int i = 0; i < characterMeshTriangles.Length - 2; i += 3)
        {
            graphEdges.Add(new Edge(graphNodes[characterMeshTriangles[i]], graphNodes[characterMeshTriangles[i + 1]]));
            graphEdges.Add(new Edge(graphNodes[characterMeshTriangles[i + 1]], graphNodes[characterMeshTriangles[i + 2]]));
            graphEdges.Add(new Edge(graphNodes[characterMeshTriangles[i]], graphNodes[characterMeshTriangles[i + 2]]));
        }
    }

    /**
     * <summary>
     * "RemoveDuplicates" function removes possible duplicate edges from "inputList" list parameter.
     * </summary>
     * 
     * <param name="inputList">
     * List that the duplicates will be deleted from.
     * </param>
     */
    void RemoveDuplicates(List<Edge> inputList)
    {
        List<Edge> toBeDeleted = new List<Edge>();
        foreach (Edge e1 in inputList)
        {
            foreach (Edge e2 in inputList)
            {
                if (!Object.ReferenceEquals(e1, e2) &&
                    ((e1.vertex1.position == e2.vertex1.position && e1.vertex2.position == e2.vertex2.position) ||
                    (e1.vertex1.position == e2.vertex2.position && e1.vertex2.position == e2.vertex1.position))
                    )
                {
                    if (!toBeDeleted.Contains(e2) && !toBeDeleted.Contains(e1))
                    {
                        toBeDeleted.Add(e2);
                    }

                }
            }
        }

        foreach (Edge edge in toBeDeleted)
        {
            inputList.Remove(edge);
        }

    }

    /**
     * <summary>
     * "SetNodeNeighbors" function assigns neighbors to graph nodes by using the edge information.
     * </summary>
     */
    void SetNodeNeighbors()
    {
        foreach (Node node in graphNodes)
        {
            foreach (Edge edge in graphEdges)
            {
                if (edge.vertex1.Equals(node))
                {
                    node.neighbors[edge.vertex2] = edge.length;
                }

                else if (edge.vertex2.Equals(node))
                {
                    node.neighbors[edge.vertex1] = edge.length;
                }

            }
        }
    }

    /**
     * <summary>
     * "CreateGraph" function creates the graph based on node and edge data.
     * </summary>
     * 
     * <param name="nodes">
     * Graph node list.
     * </param>
     * 
     * <param name="edges">
     * Graph edge list.
     * </param>
     * 
     * <returns>
     * 
     * Type: MeshGraph
     * Created graph.
     * 
     * </returns>
     */
    MeshGraph CreateGraph(List<Node> nodes, List<Edge> edges)
    {
        return new MeshGraph(nodes, edges);
    }


    /**
     * <summary>
     * "FindNodeByPosition" function finds the node with specified position information. 
     * </summary>
     * 
     * <param name="position">
     * Position that will be used as the identifier.
     * </param>
     * 
     * 
     * <returns>
     * 
     * Type: Node
     * Node with the specified position.
     * 
     * </returns>
     */
    Node FindNodeByPosition(Vector3 position)
    {
        foreach(Node node in meshGraph.nodes)
        {
            if (node.position.Equals(position))
            {
                return node;
            }
        }

        return null;
    }
    
    /**
     * <summary>
     * "DrawRoute" function is the main logic function that implements the Dijkstra algorithm application.
     * It draws the route and updates the stats based on the algorithm results. 
     * </summary>
     * 
     */
    void DrawRoute()
    {
        if (startSphere != null && endSphere != null)
        {
            //Resetting the algorithm.
            dijkstra = new Dijkstra(meshGraph);

            //Setting the source node.
            Node sourceNode = FindNodeByPosition(startSphere.transform.position);
            if (sourceNode == null)
            {
                Debug.LogError("sourceNode is null");
            }

            //Setting the destination node.
            Node destinationNode = FindNodeByPosition(endSphere.transform.position);
            if (destinationNode == null)
            {
                Debug.LogError("destinationNode is null");
            }

            //Simulating the algorithm and calculating elapsed time.
            System.DateTime startTime = System.DateTime.Now;
            dijkstra.Apply(sourceNode, destinationNode);
            System.DateTime endTime = System.DateTime.Now;
            int elapsedTime = (int)((endTime - startTime).TotalMilliseconds);

            //Getting route positions recursively and storing them sequentially.
            routePositions.Clear();
            dijkstra.SetRoutePositions(destinationNode, routePositions);

            //Updating stats.
            nodeCountText.text = string.Format("NodeCount {0}", routePositions.Count);
            calcDurationText.text = string.Format("Calc Duration: {0} ms", elapsedTime);

            //Drawing the route based on route position information
            lineObject = Instantiate(line, startSphere.transform.position, Quaternion.identity);
            lineObject.GetComponent<LineRenderer>().positionCount = routePositions.Count;
            lineObject.GetComponent<LineRenderer>().SetPositions(routePositions.ToArray());


        }
    }


    /**
     * "PlaceSphere" function visualizes the source and destination vertices.
     * Also on update of these vertices, it starts the algorithm simulation.
     * 
     * <param name="inputSphere">
     * The variable which will be holding the reference of the instantiated game object.
     * </param>
     * 
     * <param name="inputColor">
     * The color of the instantiated game object.
     * </param>
     * 
     */
    void PlaceSphere(ref GameObject inputSphere, Color inputColor)
    {
        ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        if (Physics.Raycast(ray, out hit, 200))
        {
            for (int i = 0; i < meshGraph.nodes.Count; i++)
            {
                if (Vector3.Distance(meshGraph.nodes[i].position, hit.point) < 0.05f)
                {
                    Destroy(inputSphere);
                    Destroy(lineObject);

                    inputSphere = Instantiate(sphere, meshGraph.nodes[i].position, Quaternion.identity);
                    inputSphere.GetComponent<Renderer>().material.color = inputColor;

                    DrawRoute();
                    break;
                }
            }
        }
    }

    public void QuitApplication()
    {
        Application.Quit();
    }

    void Start()
    {
        AnalyzeMeshData();
        CreateGraphNodes();
        CreateGraphEdges();
        RemoveDuplicates(graphEdges);
        SetNodeNeighbors();
        meshGraph = CreateGraph(graphNodes, graphEdges);
        dijkstra = new Dijkstra(meshGraph);
        routePositions = new List<Vector3>();
    }




    // Update is called once per frame
    void Update()
    {

        if (Input.GetMouseButtonDown(0))
        {
            PlaceSphere(ref startSphere, Color.red);
        }

        if (Input.GetMouseButtonDown(1))
        {
            PlaceSphere(ref endSphere, Color.blue);
        }


    }
}
