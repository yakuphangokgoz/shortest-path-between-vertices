# README #

## Shortest Path Between Vertices ##
Simovate İşe Alım Süreci Görevi

#### Kullanılan Yöntemler ####

Sorunun çözümünde graph teorisi ve Dijkstra en kısa yol algoritması kullanıldı.

#### Boş Proje Üzerine Eklenenler ####

* ##### Logic.cs #####
Bütün simülasyon mantığını içeren skript. Üzerinde çalışılacak olan ağın (mesh) bulunduğu modele bileşen (component) olarak atanmıştır. Gerekli bütün açıklamalar dosyanın içerisine yorum olarak eklenmiştir.  

* ##### InstructionPanel #####
Örnek projedeki talimat panelinin benzeri.

* ##### QuitButton #####
Uygulamadan çıkışı kolaylaştırma amacıyla yerleştirilmiş basit çıkış düğmesi

#### Geliştirici ####

Yakuphan Gökgöz